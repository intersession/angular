define(['angularAMD',
        'angular-animate',
        'angular-aria',
        'angular-material',
        'angular-ui-router',
        'angular-loading-bar',
        'angular-simple-logger',
        'angular-leaflet-directive',
        'angular-table',
        './factories/index',
        './controllers/index',
        './directives/index',
        './configs/index'], function(angularAMD) {

    var boutique = angular.module('boutique',
            ['ngAnimate',
             'ngAria',
             'ngMaterial',
             'ui.router',
             'angular-loading-bar',
             'nemLogging',
             'leaflet-directive',
             'ngTable',
             'app.factories',
             'app.controllers',
             'app.directives',
             'app.configs'])
        .config(function($mdThemingProvider) {
            // Extend the red theme with a few different colors
            var neonRedMap = $mdThemingProvider.extendPalette('red', {
                '500': 'E84627'
            });
            // Register the new color palette map with the name <code>neonRed</code>
            $mdThemingProvider.definePalette('neonRed', neonRedMap);
            // Use that theme for the primary intentions
            $mdThemingProvider.theme('default')
                .primaryPalette('neonRed')
                .accentPalette('neonRed')
        });
    return angularAMD.bootstrap(boutique);
});
