define(['./module'], function (directives) {

    'use strict';

    directives.directive('menu', ['dataFactory', function (dataFactory) {
      var controller = ['$scope','sessionFactory','$stateParams', function ($scope,sessionFactory,$stateParams) {
        $scope.logout = function () {
            console.log('logout');
            sessionFactory
              .destroy()
              .then(function (dataResponse) {
                console.log(dataResponse);
                  $rootScope.$broadcast('showToast', {message: "Vous êtes déconnecté"});
              });
        };
        //var id = $stateParams.id;
        //$scope.selectedIndex = $stateParams.tab;

      }];
    	return {
            restrict: "E",
            templateUrl: '../views/partials/menu.html',
            controller: controller,
            link: function (scope, element, attrs) {



                // Sidebar constructor
                //
                // -------------------
                $(document).ready(function() {

                    var sidebar = $('#sidebar');
                    var sidebarHeader = $('#sidebar .sidebar-header');
                    var sidebarImg = sidebarHeader.css('background-image');
                    var toggleButtons = $('.sidebar-toggle');

                    // Hide toggle buttons on default position
                    toggleButtons.css('display', 'none');
                    $('body').css('display', 'table');

                    // Header cover
                    $('#sidebar-header').change(function() {
                        var value = $(this).val();

                        $('.sidebar-header').removeClass('header-cover').addClass(value);

                        if (value == 'header-cover') {
                            sidebarHeader.css('background-image', sidebarImg)
                        } else {
                            sidebarHeader.css('background-image', '')
                        }
                    });
                });

                /**
                 * Add JQuery animation to bootstrap dropdown elements.
                 */

                (function($) {
                    var dropdown = $('.dropdown');

                    // Add slidedown animation to dropdown
                    dropdown.on('show.bs.dropdown', function(e){
                        $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
                    });

                    // Add slideup animation to dropdown
                    dropdown.on('hide.bs.dropdown', function(e){
                        $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
                    });
                })(jQuery);


            }
    	};
    }]);
});
