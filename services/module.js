define(['angularAMD'], function (angularAMD) {
    'use strict';
    return angular.module('app.services', []);
});