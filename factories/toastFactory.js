define(['./module'], function (factories) {

    'use strict';

    factories.factory('toastFactory', [ '$mdToast', '$rootScope',
        function($mdToast, $rootScope){
            var toastFactory = {};

            var toastPosition = {
                bottom: false,
                top: true,
                left: true,
                right: false
            };
            var getToastPosition = function () {
                return Object.keys(toastPosition)
                        .filter(function (pos) {
                            return toastPosition[pos];
                        })
                        .join(' ');
            };

            var show = function(content, delay) {
                delay = typeof delay !== 'undefined' ? delay : 1000;
                $mdToast.show(
                        $mdToast.simple()
                        .content(content)
                        .position(getToastPosition())
                        .hideDelay(delay)
                        );
            }

            $rootScope.$on( 'showToast', function(event, eventData) {
              // console.log('coucocu');
                show(eventData.message, eventData.delay);
            });

            // var toastFactory.example = function () {
            //     Public function
            // }

            return toastFactory;
        }
    ]);
});
