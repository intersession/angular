
define(['./module'], function (factories) {

    'use strict';

    factories.factory('sessionInjectorFactory', [function () {
        var sessionInjector = {};
        sessionInjector.request = function(config) {
          if (localStorage.getItem('session_token')) {
            // config.headers['x-session-token'] = localStorage.getItem('session_token');
            config.headers.authorization = localStorage.getItem('session_token');
            // console.log(config);e
          }
          return config;
        }
        return sessionInjector;
    }])
});
