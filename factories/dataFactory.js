define(['./module'], function (factories) {

    'use strict';
    factories.factory('dataFactory', ['$http','$window', '$q',
        function($http, $window, $q, $httpProvider){

            var dataFactory = {};

            var baseUrl = 'http://ipssi-lyon.fr/'

            dataFactory.setContactForm = function (currentContactForm) {
                console.log(currentContaclogintForm);
            }

            dataFactory.getSession = function(username, password){
              return $http ({
                url: baseUrl + 'api/v1/auth/login',
                method: 'POST',
                data: {
                  email: username,
                  password: password
                }
              });
            }

            dataFactory.forgotPwd = function(email) {
              return $http ({
                url: baseUrl + 'forgetpassword',
                method: 'POST',
                data: {
                  email: email
                }
              })
            }

            dataFactory.updateAccountAddress = function (address) {
              return $http ({
                url: 'http://api.twinresto.dev/', // make informations of client in account !
                method: 'GET',
                data: {
                  address: address
                }
              });
            }

            dataFactory.dropSession = function(token){
              return $http ({
                url: baseUrl + 'api/v1/auth/logout',
                method: 'POST',
                data: { "token": token }
              });
            }

            // dataFactory.updatePwdLogin = function(login, password){
            //   return $http ({
            //     url: 'http://api.twinresto.dev/update_connexion',
            //     method: 'POST',
            //     data: {
            //       login : login,
            //       password: password
            //     }
            //   });
            // }

            // dataFactory.getUser = function () {
            //   return $http ({
            //     url: 'http://api.twinresto.dev/get_user',
            //     method: 'GET'
            //   });
            // }

            dataFactory.getTableList = function () {
              return $http ({
                url: baseUrl + 'api/v1/table',
                method: 'GET'
              });
            }

            dataFactory.setNewTable = function (data) {
              return $http ({
                url: baseUrl + 'api/v1/table/',
                method: 'POST',
                data: {"data": data}
              })
            }

            dataFactory.deleteTable = function (id) {
              return $http ({
                url: baseUrl + 'api/v1/table/' + id,
                method: 'DELETE'
              })
            }

            dataFactory.modifyTable = function (id, data) {
              return $http ({
                url: baseUrl + 'api/v1/table/' + id,
                method: 'PUT',
                data: {"data": data}
              })
            }

            dataFactory.getTableContent = function (id) {
              return $http ({
                url: baseUrl + 'api/v1/table/' + id,
                method: 'GET'
              })
            }


            dataFactory.setNewEntry = function (data) {
              return $http ({
                url: baseUrl + 'api/v1/row',
                method: 'POST',
                data: {data: data}
              })
            }

            dataFactory.deleteEntry = function (id, idtable) {
              return $http ({
                url: baseUrl + 'api/v1/row/' + idtable + '/' + id,
                method: 'DELETE'
              })
            }
            dataFactory.setContactForm = function (contact) {
             return $http ({
               url: baseUrl + 'api/v1/contact',
               method: 'POST',
               data: {
                 nom: contact.nom,
                 email: contact.email,
                 prenom: contact.prenom,
                 phone: contact.phone,
                 sujet: contact.sujet,
                 message: contact.message
               }
             });
           }

            return dataFactory;

      }]);
});
