
define(['./module'], function (factories) {

    'use strict';

    factories.factory('sessionFactory', ['dataFactory', '$http', '$state', '$rootScope', 'toastFactory', function(dataFactory, $http, $state, $rootScope, toastFactory){
      var sessionFactory = {};

      sessionFactory.session_token = localStorage.getItem('session_token');
      sessionFactory.exp = new Date(localStorage.getItem('exp'));

      sessionFactory.isValid = function () {
        if (!sessionFactory.session_token && !sessionFactory.exp || sessionFactory.exp <= Date.now() ) {
          sessionFactory.session_token = null;
          sessionFactory.exp = null;
          localStorage.removeItem('exp');
          localStorage.removeItem('session_token');
          return false;
        }
        else if (sessionFactory.session_token && sessionFactory.exp || sessionFactory.exp > Date.now() ) {
          return true;
        }
      }

      sessionFactory.getSession = function (username, password) {
        return dataFactory.getSession(username, password)
          .then(function (dataResponse) {
              console.log(dataResponse.data.project.id);
            if (dataResponse.data.login === 'true') {
              // console.log(sessionFactory.exp.getTime());
              // console.log('getSession true');
              console.log('id projet = ' + dataResponse.data.project.id);
              dataResponse.data.exp = new Date(dataResponse.data.exp);
              localStorage.setItem('project_id', dataResponse.data.project.id);
              localStorage.setItem('session_token', dataResponse.data.session_token);
              localStorage.setItem('exp', dataResponse.data.exp);
              sessionFactory.session_token = dataResponse.data.session_token;
              sessionFactory.exp = dataResponse.data.exp;
                  console.log('session token : ' + localStorage.session_token);
            }
            else {
              $rootScope.$broadcast('showToast', {message: 'Erreur : mauvais identifiants'});
            }
            return dataResponse;
          });
      }

      sessionFactory.destroy = function () {
        dataFactory
          .dropSession(localStorage.session_token)
          .then(function (dataResponse) {
            if(dataResponse.data.logout === "true") {
              localStorage.removeItem('project_id');
              localStorage.removeItem('session_token');
              localStorage.removeItem('exp');
              sessionFactory.session_token = null;
              sessionFactory.exp = null;
              $state.go('public.login');
            }
          });
      }
      return sessionFactory;
    }]);
});
