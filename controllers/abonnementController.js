  define(['./module'], function (controllers) {

    'use strict';

    controllers.controller('abonnementController', ['$scope', 'dataFactory','$stateParams','$timeout','$mdDialog',
      function ($scope,dataFactory,$stateParams,$timeout,$mdDialog) {
    $scope.show = false;
    $scope.status = 'Je deviens Premium';
    $scope.bool = true;
    $scope.base_url = "http://localhost:8000/";

    /*$scope.showConfirm = function(ev) {
    $mdDialog.show({

      templateUrl: './views/partials/modalPaiementAbonnement.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
    })
    .then(function(answer) {
      $scope.bool = false;
      $scope.status = '';
    }, function() {
      $scope.bool = true;
      $scope.status = 'Je deviens Premium';
    });
  };*/
    $scope.showConfirm = function(ev) {
     var confirm = $mdDialog.confirm()
           .title('Paiement')
           .textContent('Some information will be lost.'+'<br>'+ 'Do you want to refresh?')
           .ariaLabel('Paiement')
           .targetEvent(ev)
           .ok('Confirmer')
           .cancel('Fermer');

     $mdDialog.show(confirm).then(function() {
       $scope.bool = false;
       $scope.status = '';

     }, function() {
       $scope.bool = true;
       $scope.status = 'Je deviens Premium';

     });
   };

       $scope.showDelete = function(ev) {
        var confirm = $mdDialog.confirm()
              .title('Annulation')
              .textContent('Etes-vous sûr de vouloir annuler votre abonnement. Si vous annulez votre abonnement, vous perdrez toutes vos bases de données')
              .ariaLabel('Delete')
              .targetEvent(ev)
              .ok('Confirmer')
              .cancel('Fermer');

        $mdDialog.show(confirm).then(function() {
          $scope.bool = true;
          $scope.status = 'Je deviens Premium';

        }, function() {
          $scope.bool = false;
          $scope.status = '';


        });
      };

    }]);
});
