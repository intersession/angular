define(['./module'], function (controllers) {

    'use strict';

    controllers.controller('loginController', ['$scope','$state','$mdDialog', 'sessionFactory', '$rootScope','dataFactory',
    function ($scope, $state, $mdDialog, sessionFactory, $rootScope,dataFactory) {
        if (sessionFactory.isValid()) {
          $rootScope.$broadcast('showToast', {message: 'Bienvenue'});
          $state.go("private.dashboard");
        }

        $scope.login = function () {
          sessionFactory
            .getSession($scope.login.username, $scope.login.password)
            .then(function (dataResponse) {
              console.log(dataResponse.data);
              if (dataResponse.data.login === 'true') {
                $state.go('private.dashboard');
                $rootScope.$broadcast('showToast', {message: 'Bienvenue'});
                console.log("connecté");
              }
            });
        };


        var forgotPwdConfirm = function() {
           $mdDialog.show({
             templateUrl: './views/partials/trForgotPwdConfirm.html',
             controller: function forgotPwdConfirmCtrl(scope, $mdDialog, dataFactory,$rootScope,$scope) {
               scope.cancel = function () {
                  $mdDialog.cancel();
               };
             }
           });
         };

       $scope.forgotPwd = function() {
          $mdDialog.show({
            templateUrl: './views/partials/trForgotPwd.html',
            controller: function forgotPwdCtrl(scope, $mdDialog, dataFactory,$rootScope, $scope) {
              scope.cancel = function () {
                $mdDialog.cancel();
              };
              scope.save = function() {
                dataFactory.forgotPwd(scope.email).then(function (dataResponse) {
                  $mdDialog.hide();
                  // forgotPwdConfirm();
                });
              };
            }
          });

        };

    }]);
});
