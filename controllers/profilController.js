define(['./module'], function (controllers) {

    'use strict';

    controllers.controller('profilController', ['$scope','$timeout','$http','dataFactory', '$stateParams',
    function ($scope,$timeout,$http,dataFactory,$stateParams) {
      var currentContactForm = $scope.contact = {};

        /*$scope.contact = {
            object: "",
            message: ""
        }*/

        $scope.sendFormButton = function () {
            dataFactory.setContactForm(currentContactForm);
        }
        $scope.selectedIndex = $stateParams.tab;

     
  }]);
});
