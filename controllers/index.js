define([
    './mainController',
    './loginController',
    './dashboardController',
    './layoutController',
    './profilController',
    './supportController',
    './abonnementController',
    './partenaireController',
    './demontrationController',
    './presentationController',
    './tableController',
    './tableContentController'
], function () {});
