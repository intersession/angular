  define(['./module'], function (controllers) {

    'use strict';

    controllers.controller('supportController', ['$scope', 'dataFactory','$stateParams','$timeout','leafletData','$rootScope',
      function ($scope,dataFactory,$stateParams,$timeout,leafletData,$rootScope) {
        $scope.selectedIndex = $stateParams.tab;
        $scope.contact = {};

        $scope.application = false;
        $scope.abonnement = false;
        $scope.theme3 = false;
        $scope.theme4 = false;

        $scope.toggleApplication = function() {
          $scope.application = !$scope.application;
        };
        $scope.toggleAbonnement = function() {
          $scope.abonnement = !$scope.abonnement;
        };
        $scope.toggleTheme3 = function() {
          $scope.theme3 = !$scope.theme3;
        };
        $scope.toggleTheme4 = function() {
          $scope.theme4 = !$scope.theme4;
        };
      /*      $scope.map = {
           center: {
        	latitude: 46.5132, //Position initial de la carte
        	longitude: 0.1033
           },
           zoom: 15 // de 0 à 19, 0 étant la valeur de zoom la plus forte
        };

        $scope.markers = [{
           coord: {
              latitude: 44.93, //Coordonnées où placer le point
              longitude: 4.89
           },
           email: "netapsys@netapsys.fr", //Propriété métier, pour les afficher à l'utilisateur lorsqu'il sélectionne le point par exemple
           icon: "https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi.png", //Icone personnalisée
           id: 412
        },{
           coord: {
              latitude: 46.5132,
              longitude: 0.1033
           },
           email: "netapsys@netapsys.fr",
           icon: "//developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png", //Icone personnalisée
           id: 413
        }];

        $scope.clickMarker = function(marker) {
           alert(marker.email); //Affichera l'email du point sur lequel on a cliqué
        };

*/

        angular.extend($scope,{
            center:{
              lat: 48.8624333,
              lng: 2.3368088,
                zoom: 15
            },
            markers: {
                center: {
                    lat: 48.8624333,
                    lng: 2.3368088,
                    message: "BMS",
                    focus: false,
                    draggable: false
                }
            }
          });
          $scope.sendFormButton = function (nom,email,prenom,phone,sujet,message) {
            if (typeof email != "undefined" && typeof sujet != "undefined" && typeof message != "undefined") {
              dataFactory.setContactForm($scope.contact).then(function () {
                $rootScope.$broadcast('showToast', {message: "Votre message a été envoyé !", delay: 3000});
              });
            } else {
              $rootScope.$broadcast('showToast', {message: "Veuillez renseigner tous les champs", delay: 3000});
            }
          }
    }]);
});
