define(['./module'], function (controllers) {

    'use strict';

    controllers.controller('layoutController', ['$scope', 'dataFactory','$rootScope', 'toastFactory', '$mdDialog', '$state', 'sessionFactory',
    function ($scope, dataFactory, $rootScope, toastFactory, $mdDialog, $state, sessionFactory) {

        $scope.base_url = "http://localhost:8000/";

        if (!sessionFactory.isValid()) {
          console.log("Session invalide");
          // If logged out and transitioning to a logged in page:
          // à tester avec des vraies données; avant public.login
          $state.go('public.login');
        }
        $scope.logout = function () {
            sessionFactory.destroy();
        };

    }]);
});
