define(['./module'], function (controllers) {

  'use strict';

  controllers.controller('tableContentController', ['$rootScope', '$scope', 'dataFactory','$stateParams','$timeout',
    function ($rootScope, $scope,dataFactory,$stateParams,$timeout) {


      dataFactory
        .getTableContent($stateParams.id)
        .then(function(response) {
          $scope.data = response.data;
          console.log($scope.data);
          $scope.tableauHead = $scope.data.header.columns;
          $scope.addentry = {};
          for(var index in $scope.tableauHead) {
            $scope.addentry[$scope.tableauHead[index].name] = "";
          }
        });

        $scope.addEntry = function() {
          $scope.sendEntry = {
            "id_tables": $scope.data.header.id,
            "content": $scope.addentry
          }
          dataFactory
            .setNewEntry($scope.sendEntry)
            .then(function(newResponse) {
              $scope.data = newResponse.data;
              $scope.tableauHead = $scope.data.header.columns;
              $rootScope.$broadcast('showToast', {message: $scope.data.message});
            });
  			};

        $scope.deleteEntry = function(id) {
          console.log(id);
          $scope.id_table = $scope.data.header.id
          console.log($scope.delEntry);
          dataFactory
            .deleteEntry(id, $scope.id_table)
            .then(function(response) {
              $scope.data = response.data;
              $rootScope.$broadcast('showToast', {message: $scope.data.message});
            })
        }



  }]);
});
