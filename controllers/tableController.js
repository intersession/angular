  define(['./module'], function (controllers) {

    'use strict';

    controllers.controller('tableController', ['$scope', 'dataFactory','$stateParams','$timeout', 'toastFactory', '$rootScope', '$state',
     	function ($scope,dataFactory,$stateParams,$timeout, toastFactory, $rootScope, $state) {


     		// récupère la liste des tableaux
        	dataFactory
        		.getTableList()
        		.then(function(dataResult) {
        			console.log(dataResult);
        			$scope.dataApi = dataResult.data;
        		})

        	// ajoute la possibilité de créer un tableau
        	$scope.choices = [{
        		id: 'choice1'
        	}];

			$scope.addNewChoice = function() {
				var newItemNo = $scope.choices.length+1;
				$scope.choices.push({'id':'choice'+newItemNo});
			};

			$scope.removeChoice = function(id) {
				var lastItem = $scope.choices.length-1;
				$scope.choices.splice(lastItem);
				var x = $scope.table.columns;
				delete x[id]; // suppression de l'entrée visée
    			var dernier = null;
				for(var index in x) { // décalage des entrées suivantes à la place de l'entrée supprimée
				    if (index > id) {
				    	x[index-1] = x[index];
				    	dernier = index;
				    }
				}
				delete x[dernier]; // suppression du dernier élément de l'objet.
			};


			$scope.setNewTable = function() {
        $scope.table['id_projects'] = localStorage.project_id;
				dataFactory
          .setNewTable($scope.table)
          .then(function(response) {
            console.log(response);
            $scope.dataApi = response.data.data;
            $rootScope.$broadcast('showToast', {message: response.data.message});
          });
			};

			$scope.modifyTable = function(modifTable, previous, id) {
        console.log(modifTable);
        console.log(previous);
        console.log(id);
        dataFactory
          .modifyTable(id, modifTable)
          .then(function (response) {
            console.log(response);
            $scope.dataApi = response.data.data;
          })
			};

			$scope.deleteTable = function(id) {
        console.log(id);
				dataFactory
					.deleteTable(id)
					.then(function (dataResponse) {
						console.log(dataResponse);
            $scope.dataApi = dataResponse.data[0];
						$rootScope.$broadcast('showToast', {message: dataResponse.data.message});
					});
			};

    }]);
});
