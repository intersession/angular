define(['./module'], function (configs) {

    'use strict';


    configs.config(['$stateProvider', '$urlRouterProvider', 
    function ($stateProvider, $urlRouterProvider) {
        // For any unmatched url, redirect to /connexion
        $urlRouterProvider.otherwise("/connexion");

        $stateProvider
                .state('/', {
                    templateUrl: "index.html",
                    controller: "mainController"
                })
                .state('public',{
                    abstract : true,
                    template : '<md-content ui-view layout-fill></md-content>',
                    module: 'public'
                })
                .state('private',{
                //    abstract: true,
                    templateUrl: "views/layout.html",
                    module: 'private',
                    controller: "layoutController"
                })
                .state('public.login', {
                    url: "/connexion",
                    // module: 'public',
                    templateUrl: "views/login.html",
                    controller: "loginController"
                })
                .state('public.forgotPwd', {
                    url: "/mdpoublie",
                    module: 'public',
                    templateUrl: "views/forgotPwd.html"
                })
                .state('private.dashboard', {
                    url: "/dashboard", //lorsque l'on recharge la page on reste sur la page home
                    module: 'private',
                    templateUrl: "views/dashboard.html",
                    controller: "dashboardController"
                })

                .state('private.profil',{
                    url:"/profil",
                    module: 'private',
                    templateUrl: "views/profil.html",
                    controller: "profilController"
                })

                .state('private.support', {
                    url: "/support?tab",
                    module: 'private',
                    templateUrl: "views/support.html",
                    controller: "supportController"
                })
                .state('private.abonnement', {
                    url: "/abonnement",
                    module: 'private',
                    templateUrl: "views/abonnement.html",
                    controller: "abonnementController"
                })
                .state('private.partenaire', {
                    url: "/partenaire",
                    module: 'private',
                    templateUrl: "views/partenaire.html",
                    controller: "partenaireController"
                })
                .state('private.demonstration', {
                    url: "/demonstration",
                    module: 'private',
                    templateUrl: "views/demonstration.html",
                    controller: "demonstrationController"
                })
                .state('private.presentation', {
                    url: "/presentation",
                    module: 'private',
                    templateUrl: "views/presentation.html",
                    controller: "presentationController"
                })
                .state('private.tablelist', {
                    url: "/tableaux",
                    module: 'private',
                    templateUrl: "views/tablelist.html",
                    controller: "tableController"
                })
                .state('private.tableau', {
                    url: "/tableau/:id",
                    module: 'private',
                    templateUrl: "views/tableContent.html",
                    controller: "tableContentController"
                })

    }]);
});
