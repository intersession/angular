define(['./module'], function (configs) {
    
    'use strict';
    
    configs.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
        cfpLoadingBarProvider.includeSpinner = false;
    }]);
    
});