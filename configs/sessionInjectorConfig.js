define(['./module'], function (configs) {

    'use strict';

    configs.config(['$httpProvider', function ($httpProvider) {

      $httpProvider.interceptors.push('sessionInjectorFactory');
    }]);

});
