require.config({
    baseUrl: "../",
    // alias libraries paths.  Must set 'angular'
    paths: {
        angular: 'https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular',
        'angular-animate': 'https://ajax.googleapis.com/ajax/libs/angularjs/1.4.3/angular-animate.min',
        'angular-aria': 'https://ajax.googleapis.com/ajax/libs/angularjs/1.4.3/angular-aria.min',
        text: 'https://cdnjs.cloudflare.com/ajax/libs/require-text/2.0.3/text.min',
        'angular-material': 'https://material.angularjs.org/1.1.0/angular-material.min',
        'angular-ui-router': 'https://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.2.13/angular-ui-router.min',
        'angular-loading-bar': 'https://cdnjs.cloudflare.com/ajax/libs/angular-loading-bar/0.7.1/loading-bar.min',
        // 'angular-validator': 'https://cdn.rawgit.com/turinggroup/angular-validator/master/dist/angular-validator.min',
        'angular-simple-logger': 'http://cdn.rawgit.com/nmccready/angular-simple-logger/0.0.1/dist/index',
        'angular-leaflet-directive': 'https://cdn.rawgit.com/tombatossals/angular-leaflet-directive/master/dist/angular-leaflet-directive.min',
        'angular-table': 'https://cdn.rawgit.com/esvit/ng-table/1.0.0/dist/ng-table',
        angularAMD: 'https://cdn.jsdelivr.net/angular.amd/0.2/angularAMD.min'
    },
    // Add angular modules that does not support AMD out of the box, put it in a shim
    shim: {
	//https://github.com/urish/angular-moment/issues/130
        angularAMD: ['angular'],
        'angular-animate': ['angular'],
        'angular-aria': ['angular'],
        'angular-material': ['angular', 'angular-animate', 'angular-aria'],
        'angular-ui-router': ['angular'],
        'angular-loading-bar': ['angular', 'angular-animate'],
        'angular-simple-logger': ['angular'],
        'angular-leaflet-directive': ['angular', 'angular-simple-logger'],
        'angular-table': ['angular'],
        // 'angular-validator': ['angular'],
        angular: {
            exports: "angular"
        }
    },
    // kick start application
    deps: ['app']
});
